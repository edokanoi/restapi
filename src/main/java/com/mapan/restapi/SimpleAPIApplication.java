package com.mapan.restapi;

/**
 *
 * @author edyprayitno
 */

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication; 
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

@SpringBootApplication 
public class SimpleAPIApplication extends SpringBootServletInitializer {

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(SimpleAPIApplication.class);
    }
    
    public static void main(String[] args) {
        SpringApplication.run(SimpleAPIApplication.class, args);
    }
}

package com.mapan.restapi.controller;

/**
 *
 * @author edyprayitno
 */

import java.net.URI;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.mapan.restapi.dao.ProductDAO;
import com.mapan.restapi.model.Product;
import com.mapan.restapi.model.Products;

@RestController
@RequestMapping(path = "/products")
public class ProductController 
{
    @Autowired
    private ProductDAO productDao;
    
    @GetMapping(path="/", produces = "application/json")
    public Products getProducts() 
    {
        return productDao.getAllProducts();
    }
    
    @PostMapping(path= "/", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> addEmployee(
                        @RequestHeader(name = "X-COM-PERSIST", required = true) String headerPersist,
                        @RequestHeader(name = "X-COM-LOCATION", required = false, defaultValue = "ASIA") String headerLocation,
                        @RequestBody Product product) 
                 throws Exception 
    {       
        //Generate resource id
        Integer id = productDao.getAllProducts().getProductList().size() + 1;
        product.setId(id);
        
        //add resource
        productDao.addProduct(product);
        
        //Create resource location
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                                    .path("/{id}")
                                    .buildAndExpand(product.getId())
                                    .toUri();
        
        //Send location in response
        return ResponseEntity.created(location).build();
    }
}

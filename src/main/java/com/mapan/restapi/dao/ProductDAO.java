package com.mapan.restapi.dao;

/**
 *
 * @author edyprayitno
 */

import org.springframework.stereotype.Repository;

import com.mapan.restapi.model.Product;
import com.mapan.restapi.model.Products;

@Repository
public class ProductDAO 
{
    private static Products list = new Products();
    
    static 
    {
        list.getProductList().add(new Product(1, "Baking Pan", "Standart", 117000));
        list.getProductList().add(new Product(2, "Hand Blender", "Standart", 92000));
        list.getProductList().add(new Product(3, "Loker Dapur", "Standart", 229900));
    }
    
    public Products getAllProducts() 
    {
        return list;
    }
    
    public void addProduct(Product product) {
        list.getProductList().add(product);
    }
}

package com.mapan.restapi.model;

/**
 *
 * @author edyprayitno
 */

public class Product {

    public Product() {

    }

    public Product(Integer id, String productName, String modelName, Number manufacture) {
        super();
        this.id = id;
        this.productName = productName;
        this.modelName = modelName;
        this.price = manufacture;
    }
 
    private Integer id;
    private String productName;
    private String modelName;
    private Number price;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getModelName() {
        return modelName;
    }

    public void setLastName(String modelName) {
        this.modelName = modelName;
    }

    public Number getPrice() {
        return price;
    }

    public void setPrice(Number manufacture) {
        this.price = manufacture;
    }

    @Override
    public String toString() {
        return "Product [id=" + id + ", productName=" + productName + ", modelName=" + modelName + ", price=" + price + "]";
    }
}
